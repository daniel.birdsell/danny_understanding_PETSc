# README

This directory contains Daniel Birdsell's presentation about parallel computing (focused on PETSc), which is where a reader should start.  Also included are example homework problems and solutions, example PETSc codes, and Mr. Birdsell's notes.

### Documentation

The Makefile and README.md can generate documentation locally by typing 'make'.

### Codes

These codes address simple things like 'how do I put a number into a vector?' and 'how do a view that vector?'.  This was helpful to understanding the excellent, but more complicated, example problems that come when downloading PETSc.


Codes include:

1) vec_populate.F90 - declare vectors, view them, multiply them, populate them with a number of commands
2) Axy_solve.F90 - declare matricies, view them, multiply them, populate them with a number of commands.  Find y = Ax for A,x known.
3) ksp.F90 - solve a simple system of equations. 
4) snes.F90 - nonlinear solve of syste of equations.  Can also be compared to matlab code.
