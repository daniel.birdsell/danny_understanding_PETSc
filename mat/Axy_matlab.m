% Example problem matching the Axy_solve.F90 problem
%
% Defines matrix A and vector x.
% Calculates y = Ax
%

clear, clc
x=linspace(1,6,6);
fprintf('Define x and A:\n')
x=x'

for i=1:length(x)
    A(i,i) =1;
end
A(2,1) = 1;
A(3,2) = -1;
A(5,4) = -1;
A(6,5) = -1

fprintf('Multiply together to find y:\n')

y=A*x
