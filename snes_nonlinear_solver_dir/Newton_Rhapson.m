% Iteratively Solve for zeros of a system of non-linear equations in multiple variables
% Uses the Netwon-Rhapson iteration
%
% System is taken from Num Meth course:
%   f1 = u1*u2-10
%   f2 = u1/u2-2
%

clear, clc
u = [4;2] %initial guess
f = u;
tol = 1e-4;
err = 999;
it = 0;
while err>tol
    it = it +1;
    %calculate Jacobian and rhs vector
    J(1,1) = u(2);
    J(1,2) = u(1);
    J(2,1) = 1/u(2);
    J(2,2) = -u(1)/u(2);
    f(1) = u(1)*u(2)-10;
    f(2) = u(1)/u(2) -2;
    %Solve J*du=-f
    du = J\-f;
    %Update
    u = u+du;
    err = max(abs(f));
end

[it, err]
u