% Matches ex1.c in $PETSC_DIR/src/snes/examples/tutorials
%
%Iteratively Solve for zeros of a system of non-linear equations in multiple variables
% Uses the Netwon-Rhapson iteration
%
% System is:
%   f1 = u1^2+u1*u2-3
%   f2 = u1*u2+u2^2-6
%

clear, clc
u = [4;2]; %initial guess
du = [0;0];f=[0;0]
tol = 1e-5; it = 0; err = 999;
while err>tol
    it = it +1; %iteraiton counter
    %calculate Jacobian and rhs vector
    J(1,1) = 2*u(1) + u(2);
    J(1,2) = u(1);
    J(2,1) = u(2);
    J(2,2) = u(1) + 2*u(2);
    f(1) = u(1)^2+u(1)*u(2)-3;
    f(2) = u(1)*u(2)+u(2)^2-6;
    %Solve J*du=-f
    f = -f;
    du = J\f;
    %Update
    u = u+du;
    err = max(abs(f));
end

[err, it]

f
u