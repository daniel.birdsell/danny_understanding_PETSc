function [ x ] = thomas( a,b,c,r )
%Thomas Algorithm to solve a system of eqns [A]x = r for tri-diagonal
%matrix [A]
%a(2:J) is the first subdiagonal of matrix A, a(1) = 0. b(1:J) is the main diagonal of A. 
%c(1:J-1) is the first superdiagonal of A, c(J) = 0. r is the RHS vector.  x is the vector
%that is returned.
%Three steps are used in the algorithm:
%1)LU Decomposition into [L] = f(i) on main diagonal, e(i) on sub-diagonal
%and [U] = g(i) on superdiagonal.
%2) Forward solve [L]y = r
%3) Backward solve [U]x = y

%define J and preallocate
J = length (b);
e = zeros(J,1); f = e; g = e; y = e;

%LU Decomposition:
f(1) = b(1); g(1) = c(1)/f(1);
for j = 2:J
    e(j) = a(j);
    f(j) = b(j) - e(j)*g(j-1);
    g(j) = c(j)/f(j);
end

%Forward Solve
y(1) = r(1)/f(1);
for j = 2:J
    y(j) = (r(j)-e(j)*y(j-1))/f(j);
end

%Backward solve
x(J) = y(J);
for j = J-1:-1:1
    x(j) = (y(j)-g(j)*x(j+1));
end

end
