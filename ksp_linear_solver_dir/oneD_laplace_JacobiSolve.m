% Solve 1D Laplace or Poisson Equation using iterative Jacobi method
% and the direct method using the Thomas algorithm (i.e. LU decomposition)
% 
% Equation is d2u/dx2 = f(x)
% where f(x) = 0 for Laplace and f(x) = x for Poisson.
% BCs: u(x=0) = 0, u(x=1) = 1
% Analytical solution: u(x) = x (Laplace)
% Analytical solution: u(x) = x^3/6 + x*5/6 (Poisson)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is useful to understand because Ax=b is generally solved iteratively
% rather than using a direct solver in PETSc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear, clc, close all
% first select if you want to solve Poisson's equation or Laplacee Equaiton
Laplace = 1; %set to 1 for Laplace.  Anything else for Poisson. 

x = [0:0.01:1]';
dx = x(2)-x(1);
BC1 = 0; BC2 = 1;
u = 0.5*ones(length(x),1); u(1) = BC1; u(end) = BC2;%initial guess
uold = u;
errorvec = u; %initialize u
err = 999; %initialize error with dummy value larger than tol
tol = 0.001;
%set up RHS vector
if Laplace == 1
    r = zeros(length(x),1);
    r(end) = 1;
else 
    r = x;
end
it = 0;
NX = length(x);
mu = 1/dx^2;


figure(1); hold on;
tic
% this is the Jacobi iteration
while err>tol
    it = it +1; 
    u(1) = 0; u(end) = 1; %apply BCs
    u(2:NX-1) = (r(2:NX-1) - mu*uold(1:NX-2) - mu*uold(3:NX))/(-2*mu); %calculate u for current iteration based on the u values around it
    
    errorvec(1) = u(1) - BC1; errorvec(end) = u(end) - BC2;
    errorvec(2:NX-1) = mu*u(1:NX-2)-2*mu*u(2:NX-1)+mu*u(3:NX)-r(2:NX-1);
    err = max(abs(errorvec));
    [it, err]
    if it == 1 | it == 10 | it == 100 | it == 1000
        plot(x,u,'.');
    end
    uold = u;
end
toc
%calculate analytical solution
if Laplace == 1
    u_anal = x;
else
    u_anal = x;
    u_anal(1:NX) = x(1:NX).^3/6 + x(1:NX)*5/6;
end

% Now solve directly using thomas algorithm

b = -2*mu*ones(length(x),1);
b(1) = 1; b(end) = 1;

a = mu*ones(length(x),1); a(end) = 0; a(1) = 999;
c = mu*ones(length(x),1); c(1) = 0;  c(end) = 999;

u_thomas = thomas(a,b,c,r);

% Now plot
plot(x,u,'*'); xlabel('x'); ylabel('u'); title('1D Poisson Solution')
plot(x,u_thomas,'k^')
plot(x,u_anal,'-')
legend('it  1','it = 10','it = 100','it = 1000', 'Iterative Solution', 'Direct Solution','Analytical Solution')
